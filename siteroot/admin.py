# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from models import Pipeline


class PipelineAdmin(admin.ModelAdmin):
    """ Class that registers the Pipeline table for the admin views """
    list_display    = ['user', 'video_file_name', 'start_date_time', 'finished', 'log_file_name', 'result_file_name']

admin.site.register(Pipeline, PipelineAdmin)

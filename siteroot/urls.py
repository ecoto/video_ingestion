from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
import views
import api

urlpatterns = [
    # views
    url(r'^$', views.home, name='home'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^submit/$', views.submit_view, name='submit_view'),
    url(r'^records/$', views.records_view, name='records_view'),

    # api
    url(r'^submit_video$', api.submit_video, name='submit_video'),
    url(r'^get_file$', api.get_file, name='get_file'),
]

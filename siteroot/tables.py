from django.conf import settings
from django.utils.html import format_html
import django_tables2 as tables
from django_tables2.export.views import ExportMixin
from models import Pipeline
import pytz


class PipelineTable(ExportMixin, tables.Table):
    """ Class that controls de rendering of table Pipeline """
    base_url = None
    local_tz = pytz.timezone(settings.TIME_ZONE)
    export = False


    class Meta:
        """ Class to overwrite meta information on the parent class """
        model = Pipeline
        template_name = 'django_tables2/bootstrap4.html'


    def utc_to_local(self, utc_dt):
        """ UTC to LOCAL time converter """
        local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(self.local_tz)
        return self.local_tz.normalize(local_dt) # .normalize might be unnecessary


    def render_start_date_time(self, value):
        """ Customized renderer to convert the date-time and return it as a string on a specific format """
        return self.utc_to_local(value).strftime("%d-%m-%Y %H:%M:%S")


    def render_log_file_name(self, value):
        """ Customized renderer to display the log file name as a text or as a link to the log file """
        if self.export:
            return value

        return format_html('<a href="{}get_file?fname={}" target="__blank" title="open this log in a new tab"><i class="fa fa-external-link-alt"></i></a>', self.base_url, value)


    def render_result_file_name(self, value, record):
        """ Customized renderer to display the result value as a text or as a link to the result file """
        if not record.finished:
            if self.export:
                return 'N/A'
            return format_html('<span style="color:blue" title="Not yet available">N/A</span>')

        if self.export:
            return value

        if 'Error' in value or 'EXCEPTION' in value:
            return format_html('<span style="color:red">{}</span>', value)

        return format_html('<a href="{}get_file?fname={}" target="__blank" title="open this result in a new tab"><i class="fa fa-external-link-alt"></i></a>', self.base_url, value)


    def render_finished(self, value):
        """ Customized renderer to display the finished boolean as an icon """
        if self.export:
            return value

        if value == True:
            return format_html('<i class="fa fa-check" style="color:green" title="Finished"></i>')
        else:
            return format_html('<i class="fa fa-spinner" style="color:blue" title="Running"></i>')

